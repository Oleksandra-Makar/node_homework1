const express = require('express');
const morgan = require('morgan');

const app = express();
const PORT = 8080;

const apiRouter = require('./apiRouter');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/files', apiRouter);

app.listen(PORT, () => {
  console.log(`Server works at http://localhost:${PORT}`);
});
