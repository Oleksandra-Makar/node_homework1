const express = require('express');
const fs = require('fs');

const router = express.Router();

router.post('/', (req, res) => {
    const supportedExtensionsRegExp = /(log|txt|json|yaml|xml|js)$/g;
    const requestType = req.headers['content-type'];
    const {filename, content} = req.body;

    if (requestType !== 'application/json') {
        return res.status(400).json({message: 'The content type should be "application/json"!'})
    }
    if (!filename || !content) {
        return res.status(400).json({message: 'Please enter filename and content you want to create!'})
    }
    if (!filename.match(supportedExtensionsRegExp)) {
        return res.status(400).json({message: 'You did not provide file extension or provided file extension is not supported'})
    }
    (async () => {
        try {
            const filesData = await fs.readdir('files');

            if (filesData.includes(filename)) {
                return res.status(400).json({
                    message: `File ${filename} is already exists. Please enter another filename!`
                })
            }
            await fs.writeFile(`files/${filename}`, content);
            res.status(200).json({message: `File created successfully`});
        } catch (error) {
            res.status(500).json({message: 'Server error'});
            console.log(error)
        }
    })()
});



router.get('/', function (req, res) {
  fs.readdir('./files/', (err, files) => {
    if (err) {
      return res.status(400).json({ message: 'Client error' });
    }

    res.status(200).json({ message: 'Success', files: files });
  });
});

router.get('/:filename', (req, res) => {
  const filename = req.params.filename;

    (async () => {
        try {
            const filesData = await fs.readdir('files');

            if (!filesData.includes(filename)) {
                return res.status(400).json({
                    message: `No file with '${filename}' filename found`
                })
            }

            const content = await fs.readFile(`files/${filename}`, 'utf-8');
            const extension = filename.match(/(log|txt|json|yaml|xml|js)$/g)[0];
            const uploadedDate = (await fs.stat(`files/${filename}`)).birthtime;

            res.status(200).json({message: "Success", filename, content, extension, uploadedDate});
        } catch (error) {
            res.status(500).json({message: 'Server error'});
            console.log(error)
        }
    })()
});

router.delete('/:filename', (req, res) => {
  const filename = req.params.filename;

  fs.unlink(`./files/${filename}`, (error, file) => {
    if (error) {
      return res.status(404).json({ message: `File ${filename} does not exist` });
    }

    res.status(200).json({ message: `File ${filename} was successfully deleted` });
  });
});

router.put('/:filename', (req, res) => {
  const filename = req.params.filename;
  const { content } = req.body;

  fs.writeFile(`files/${filename}`, content, { flag: 'r+' }, (error, file) => {
    if (error) {
      return res.status(400).json({ message: `File ${filename} does not exist` });
    }

    res.status(200).json({ message: `File ${filename} was successfully modified` });
  });
});

module.exports = router;
